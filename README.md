# Python Web Conference 2022

## Let's Forget About Excel: <br> Conda + Jupyter + Pandas to Leave Office Behind

![Event Hero](images/event_hero.png)

### [Event Link](https://pwc2022.loudswarm.com/session/lets-forget-about-excel-conda-jupyter-pandas-to-leave-office-behind)

### [Repository](https://gitlab.com/soldavid/pwc20220323/)

Excel? Where we're going, we don't need Excel. You can leave Office and the Spreadsheet behind. You can use Python, Conda, Jupiter, and Pandas to quickly load, analyze and export your data in a better way. No more memory limits, strange functions, and manual processes. Let's go to the future.

This is a 3-hour workshop to give a Python beginner a walk into Pandas.

We'll learn:

### About Python:

* How to install Conda for Python, Pandas, and all of its Data processing ecosystem.
* How to use Jupyter Lab and Jupiter Notebooks.
* How to use Pandas for data analysis.

### About Pandas:

* Import and Export data.
* Analyze and clean data.
* Filtering.
* Data Processing.
* Simple plotting.

### About Jupyter:

* Simple Markdown.
* Sharing Notebooks.
* Exporting as HTML or PDF.

### Files

* Main talk Jupyter Notebook: `pwc20220323.ipynb`
* Markdown Mermaid Example: `mermaid_example.md`
* Python cells example: `sample_cells.py`

### David Sol

Living in the land of the Mexica with his wife, daughter and 4 university-graduated rabbits, has discovered a new purpose in Cloud Computing. SRE @ Wizeline, AWS Certified DevOps Engineer Professional and AWS Community Builders group member. He believes Cloud Computing, DevOps and Automation are the future of our profession, and wishes to spread the good news all around. They planted and we ate. Let's plant, so that they eat.

* Twitter: [@soldavidcloud](https://twitter.com/soldavidcloud)
* LinkedIn: [Profile](https://www.linkedin.com/in/david-jonathan-sol-llaven-5b814a225/)
